import { Component, OnInit, NgModule, Injectable } from '@angular/core';
import { NgForm, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpServiceService } from '../http-service.service';


@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})


@Injectable()
export class ContactFormComponent implements OnInit {
POSTUrl = '';

constructor(private httpService: HttpServiceService) {

    this.POSTUrl = 'https://ehzk6mgbt5.execute-api.us-east-1.amazonaws.com/dev/person';
  }
ngOnInit() {

    }

sendToAPI(userInfo: NgForm) {
      let JSONPost = JSON.stringify(userInfo.value);
      console.log(userInfo.value);
      console.log(JSONPost);
      this.httpService.postContact(this.POSTUrl, JSONPost);
      console.log(this.httpService.postContact(this.POSTUrl, JSONPost));
    }

}

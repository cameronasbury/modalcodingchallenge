import { Component, OnInit, NgModule } from '@angular/core';
import { NgForm, FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [

    FormsModule,

    ReactiveFormsModule,

  ]
})

@Component({
  selector: 'app-contact-form-modal',
  templateUrl: './contact-form-modal.component.html',
  styleUrls: ['./contact-form-modal.component.css']
})
export class ContactFormModalComponent implements OnInit {

  constructor() {

  }

  ngOnInit() {

   const modal = document.getElementById('contactModal');

   const btn = document.getElementById('contactBtn');

   const xBtn: HTMLButtonElement = document.getElementsByClassName('closeBtn')[0] as HTMLButtonElement;


   btn.onclick = () => {
     modal.style.display = 'block';
   };

   xBtn.onclick = () => {
     modal.style.display = 'none';
   };

   window.onclick = (event) => {
     if (event.target === modal) {
       modal.style.display = 'none';
     }
   };

  }

}





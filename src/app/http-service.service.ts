import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/';
import { map, catchError } from 'rxjs/operators';
import {  HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {



  constructor(private http: HttpClient ){}

  postContact<T> (url: string, json: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      }),
      observe: 'response' as 'body'
  };
    //console.log(this.http.post(url, json, httpOptions).pipe(catchError(this.handleErrorObservable)));
    return this.http.post(url, json, httpOptions).pipe(catchError(this.handleErrorObservable));
  }

  extractData(res: Response) {
    let body = res.json();
    return body || {};
  }
  handleErrorObservable (error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }
}

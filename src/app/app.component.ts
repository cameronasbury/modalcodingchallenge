import { Component } from '@angular/core';
import { ContactFormModalComponent } from './contact-form-modal/contact-form-modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ModalCodingChallenge';
}
